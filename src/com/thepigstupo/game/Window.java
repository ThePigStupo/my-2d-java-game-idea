package com.thepigstupo.game;

import javax.swing.JFrame;

public class Window extends JFrame {

    public Window() {
        setTitle("Client 1.0  (Made by ThePigStupo)");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setContentPane(new GamePanel(1280, 720));

        pack();
        setVisible(true);
    }

}
